
import decentralisedmodel.data.InputData;
import decentralisedmodel.model.Service;
import decentralisedmodel.model.ServiceProvider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class Requester {

    static List<ServiceProvider> serviceProviderPool = new ArrayList<>();
    
    public static void main(String[] args) {
        
        ServiceProvider spTOPK = new ServiceProvider();
        ServiceProvider spRANDOMK = new ServiceProvider();
        ServiceProvider spSP1A = new ServiceProvider();
        ServiceProvider spSP1B = new ServiceProvider();
        ServiceProvider spSP2A = new ServiceProvider();
        ServiceProvider spSP2B = new ServiceProvider();
        
        List<Service> servicePool1 = InputData.createServices(InputData.serviceClasses[0], InputData.inputOutput[0][0], InputData.inputOutput[0][1]);
        spTOPK.setServices(convertToMap(servicePool1));
        
        List<Service> servicePool2 = InputData.createServices(InputData.serviceClasses[1], InputData.inputOutput[1][0], InputData.inputOutput[1][1]);
        spRANDOMK.setServices(convertToMap(servicePool2));
        
        List<Service> servicePool3 = InputData.createServices(InputData.serviceClasses[2], InputData.inputOutput[2][0], InputData.inputOutput[2][1]);
        spSP1A.setServices(convertToMap(servicePool3));
        
        List<Service> servicePool4 = InputData.createServices(InputData.serviceClasses[2], InputData.inputOutput[2][0], InputData.inputOutput[2][1]);
        spSP1B.setServices(convertToMap(servicePool4));
        
        List<Service> servicePool5 = InputData.createServices(InputData.serviceClasses[3], InputData.inputOutput[3][0], InputData.inputOutput[3][1]);
        spSP2A.setServices(convertToMap(servicePool5));
        
        List<Service> servicePool6 = InputData.createServices(InputData.serviceClasses[3], InputData.inputOutput[3][0], InputData.inputOutput[3][1]);
        spSP2B.setServices(convertToMap(servicePool6));
        
//        System.out.println("services");
        
        serviceProviderPool.add(spRANDOMK);
        serviceProviderPool.add(spTOPK);
        serviceProviderPool.add(spSP1A);
        serviceProviderPool.add(spSP1B);
        serviceProviderPool.add(spSP2A);
        serviceProviderPool.add(spSP2B);
    }
    
    
    public static void request(String input, String output) {
        //"a", "d"
        
        // Broadcast to all service providers
//        for(ServiceProvider sp: serviceProviderPool) {
//            sp.discoverServices(String input, String output);
//        }
    }
    
    
    public static Map<String, Service> convertToMap(List<Service> serviceList) {
        Map<String, Service> map = new HashMap<>();
        for(Service service: serviceList) {
            map.put(service.getID(), service);
        }
        return map;
    }
    
    
}
