package gocomo.messages;

/**
 * Created by NanxiChen on 9/5/2015.
 */
public class CompositionRequest extends Builders{
    public void buildType()   {
        gMsg.setType("<type>req</type>");
    }
    public void buildHead(int id, String  client)   {
        String h1=Integer.toString(id);
        String h2=client;
        gMsg.setHead("<id>"+h1+"</id><client>"+h2+"</client>");
    }
    public void buildContent(String c)  {
        String[] cl=c.split("[|]");
        //String c1=c.substring(c.indexOf("+"));
        gMsg.setContent("<input>"+cl[0]+"</input><output>"+ cl[1]+"</output><sender>"+ cl[2]+"</sender><qos>"+ cl[3]+"</qos>");
    }
    public void buildExtra(String e) {
        gMsg.setExtra(e);
    }
}
