package gocomo.messages;

/**
 * Created by NanxiChen on 9/5/2015.
 */
public class ExecutionTokenBuilder extends Builders{
    public void buildType()   {
        gMsg.setType("<type>exet</type>");
    }
    public void buildHead(int id, String client)   {
        String h1=Integer.toString(id);
        String h2=client;
        gMsg.setHead("<id>"+h1+"</id><client>"+h2+"</client>");
    }
    public void buildContent(String c)  {
        String[] cl=c.split("[|]");
        //String c1=c.substring(c.indexOf("+"));
        gMsg.setContent("<input>"+cl[0]+"</input>");
    }
    public void buildExtra(String e) {
        String[] el=e.split("[|]");
        gMsg.setExtra("<waypoint>"+el[0]+"</waypoint><guide>"+ el[1]+"</guide>");
    }
}
