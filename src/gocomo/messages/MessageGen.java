package gocomo.messages;

/**
 * Created by NanxiChen on 9/5/2015.
 */
public class MessageGen {
    private Builders msgBuilder;

    public void setMsgBuilder (Builders mb) { msgBuilder = mb; }
    public GocomoMessage getMessage() { return msgBuilder.getMsg(); }
    public String getMessageString() { return msgBuilder.getMsgString(); }

 /** build discovery msg, complete message invocation message, execution message with extra data*/
    public void constructMessage(int id, String client,String c,String e) {
        msgBuilder.createNewMessage();
        msgBuilder.buildType();
        msgBuilder.buildHead(id, client);
        msgBuilder.buildContent(c);
        msgBuilder.buildExtra(e);
    }

    /** build advertisement message*/
    public void constructMessage(String c,String e) {
        msgBuilder.createNewMessage();
        msgBuilder.buildType();
        msgBuilder.buildContent(c);
        msgBuilder.buildExtra(e);
    }
    /** build discovery msg, complete message invocation message, execution message without extra data, and composition requet*/
    public void constructMessage(int id, String client,String c) {
        msgBuilder.createNewMessage();
        msgBuilder.buildType();
        msgBuilder.buildHead(id, client);
        msgBuilder.buildContent(c);
    }
}
