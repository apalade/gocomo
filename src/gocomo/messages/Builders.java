package gocomo.messages;

/**
 * Created by NanxiChen on 9/5/2015.
 */
public abstract class Builders {
    
    protected GocomoMessage gMsg;
    public GocomoMessage getMsg() { return gMsg; }
    public String getMsgString() { return gMsg.getStringMsg(); }
    public void createNewMessage() { gMsg = new GocomoMessage(); }

   // public abstract void buildHead();
    public abstract void buildType();
    public abstract void buildHead(int id, String client);
    public abstract void buildContent(String c);
    public abstract void buildExtra(String e);
}
