package gocomo.messages;

/**
 * Created by NanxiChen on 9/5/2015.
 */
public class ServiceAdvertisementBuilder extends Builders{
    public void buildType()   {
        gMsg.setType("<type>advt</type>");
    }
    public void buildHead(int id, String client)   {
        gMsg.setHead("");
    }
    public void buildContent(String c)  {
        String[] cl=c.split("[|]");
        //String c1=c.substring(c.indexOf("+"));
        gMsg.setContent("<output>"+cl[0]+"</output><sender>"+ cl[2]+"</sender>");
    }
    public void buildExtra(String e) {
        gMsg.setExtra("");
    }
}
