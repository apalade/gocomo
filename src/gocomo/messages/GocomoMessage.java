package gocomo.messages;

/**
 * Created by NanxiChen on 9/5/2015.
 */
public class GocomoMessage {
    private String type = "";
    private String head = "";
    private String content = "";
    private String extra = "";
    private String msg = "";

    public void setNewMsg (String msg){
        int msgLength=msg.length();
        String t = "";
        String h = "";
        String c = "";
        String e = "";

        if (msg.indexOf("<msg_extra>")!=-1)e=msg.substring(msg.indexOf("<msg_extra>")+11, msg.indexOf("</msg_extra>"));
        this.extra =e;
        if (msg.indexOf("<msg_content>")!=-1)c=msg.substring(msg.indexOf("<msg_content>")+13, msg.indexOf("</msg_content>"));
        this.content =c;
        if (msg.indexOf("<msg_head>")!=-1)h=msg.substring(msg.indexOf("<msg_head>")+10, msg.indexOf("</msg_head>"));
        this.head = h;
        if (msg.indexOf("<msg_type>")!=-1)t=msg.substring(msg.indexOf("<msg_type>")+10, msg.indexOf("</msg_type>"));
        this.type=t;

    }
    public void setType (String type)     { this.type = type; }
    public void setHead (String head)     { this.head = head; }
    public void setContent (String content)     { this.content = content; }
    public void setExtra (String extra) { this.extra = extra; }
    public String getStringMsg () { 
        return  "<msg_type>"+type+"</msg_type><msg_head>"+head+"</msg_head><msg_content>"+content+"</msg_content><msg_extra>"+extra+"</msg_extra>"; 
    }
}
