package gocomo.utils;

import gocomo.Direction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by NanxiChen on 9/8/2015.
 */
public class JsonUtil {


    public static String toJSon( HashMap<String, List<Direction>> guidepostMap) {

        try {
            // Here we convert Java Object to JSON
            JSONObject jsonGuideSet = new JSONObject();
            Iterator i= guidepostMap.entrySet().iterator();
            while (i.hasNext()){
                Object o=i.next();
                String key=o.toString();

                //create a guidepost object
                List<Direction> guide=new ArrayList<>();
                JSONObject jsonGuidepost = new JSONObject();
                int b=guidepostMap.size();
                boolean c=guidepostMap.containsKey(key);

                guide.addAll(guidepostMap.get(key));
                //for (Direction direction : guide  ) {
                int a=guide.size();
                    Direction direction=guidepostMap.get(key).get(0);
                    JSONObject jsonDirect = new JSONObject();

                    jsonDirect.put("did", direction.getId());

                    jsonDirect.put("postCondition", direction.getPostCondition());
                    jsonDirect.put("waypoint", direction.getWaypoint());
                    jsonDirect.put("qos", direction.getQos());
                    jsonGuidepost.put(String.valueOf(direction.getId()),jsonDirect);

               // }

                //put a guidepost into the guidepost set
                jsonGuideSet.put(key, jsonGuidepost);
            }

            return jsonGuideSet.toString();

        } catch(JSONException ex) {

            ex.printStackTrace();

        }
        return null;

    }

    public static String toJSonGuidepost( List<Direction> guidepost) {

        try {
            // Here we convert Java Object to JSON
            JSONArray jsonGuideSet = new JSONArray();

               for (Direction direction : guidepost ) {

                JSONObject jsonDirect = new JSONObject();

                   jsonDirect.put("did", direction.getId());
                   jsonDirect.put("postCondition", direction.getPostCondition());
                   jsonDirect.put("waypoint", direction.getWaypoint());
                   jsonDirect.put("qos", direction.getQos());

                   jsonGuideSet.put(jsonDirect);
                }


            return jsonGuideSet.toString();

        } catch(JSONException ex) {

            ex.printStackTrace();

        }
        return null;

    }
    
    // convert physical guidepost to guidepost object
    public static   List<Direction> toGuidepost(String data)  {
        //JSONObject guidepostObj = new JSONObject(data);
        List<Direction> guidepost=new ArrayList<Direction>();
        try{
        JSONArray jArr = new JSONArray(data); //guidepostObj.getJSONArray("list");
        for (int i=0; i < jArr.length(); i++) {
            //JSONObject directionObj = guidepostObj.getJSONObject(String.valueOf(i));
            JSONObject directionObj =jArr.getJSONObject(i);
            Direction direction=new Direction(directionObj.getInt("did"), directionObj.getString("postCondition"),directionObj.getString("waypoint"), directionObj.getInt("qos"));
            guidepost.add(direction);
        }
        } catch(JSONException ex) {
            System.err.println(ex.getLocalizedMessage());
        }
        return guidepost;
    }
    public static  HashMap<String, List<Direction>> toGuidepostMap(String data) throws JSONException {
        JSONObject jsonGuideSet = new JSONObject(data);
        HashMap<String, List<Direction>> guidepostMap=new HashMap<>();

        Iterator keysToCopyIterator = jsonGuideSet.keys();
        //List<String> keysList = new ArrayList<String>();
        while(keysToCopyIterator.hasNext()) {
            String key = (String) keysToCopyIterator.next();
            if (key.startsWith("rid_")){
                JSONObject guidepostObj = jsonGuideSet.getJSONObject(key);
                List<Direction> guidepost=new ArrayList<Direction>();
                JSONArray jArr = guidepostObj.getJSONArray("list");
                for (int i=0; i < jArr.length(); i++) {
                    JSONObject directionObj = guidepostObj.getJSONObject(key);
                    Direction direction=new Direction();

                    guidepost.add(direction);
                }

                guidepostMap.put(key, guidepost);
            }

        }


      return guidepostMap;
    }



    public static String toJSonGuidepostCount( HashMap<String, Integer> guidepostCount) {

        try {
            // Here we convert Java Object to JSON
            JSONObject jsonGuideCount = new JSONObject();
            Iterator i= guidepostCount.entrySet().iterator();
            while (i.hasNext()){
                Object o=i.next();
                String key=o.toString();
                jsonGuideCount.put(key, guidepostCount.get(key));
            }
            return guidepostCount.toString();

        } catch(JSONException ex) {

            ex.printStackTrace();
        }
        return null;
    }

    public static HashMap<String, Integer> toGuidepostCount(String data) throws JSONException {
        JSONObject jsonGuideCount = new JSONObject(data);
        HashMap<String, Integer> guidepostCount=new HashMap<>();

        Iterator keysToCopyIterator = jsonGuideCount.keys();
        //List<String> keysList = new ArrayList<String>();
        while(keysToCopyIterator.hasNext()) {
            String key = (String) keysToCopyIterator.next();
            if (key.startsWith("rid_")){
                int guidepostObj = jsonGuideCount.getInt(key);
                guidepostCount.put(key, guidepostObj);
            }
        }
        return guidepostCount;
    }
}
