package gocomo.utils;

import java.util.Arrays;

/**
 * Created by NanxiChen on 8/23/2015.
 */
public class Matchmaker {
    public static String match(String service, String req){
        String matchresult="unusable";
        String[] serviceList=service.split("[+]");
        String[] reqList=req.split("[+]");
        //if reqList fully belongs to service output returns usable, if there is entities (part of) in reqList can not match, returns usablePartial,
        int matched=0;
        for(String i:reqList) {
            if (Arrays.asList(serviceList).contains(i)) {
                matchresult = "partusable";
                matched++;
            }
        }
        if(matched==reqList.length){
            matchresult="usable";
        }
        return  matchresult;
    }

}
