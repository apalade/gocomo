package gocomo.communication.vendors;

import gocomo.communication.CommDevice;
import gocomo.communication.CommMessage;
import gocomo.communication.CommService;
import gocomo.communication.ResponseHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgroups.Address;

import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;

/**
 *
 * @author Andrei
 */
public class JGroupsCommService extends ReceiverAdapter implements CommService  {
    
    private ResponseHandler handler;
    
    private JChannel channel;
    
    private List<Address> members;
    
    private int state;
    
    public JGroupsCommService(ResponseHandler h) {
        handler = h;
        state = CommService.STATE_NONE;
        
        connect();
    }

    @Override
    public int getState() {
        return state;
    }
    
    @Override
    public void connect() {
        try {
            channel=new JChannel(); // use the default config, udp.xml
            channel.setReceiver(this);
            channel.connect("ChatCluster");
            state = CommService.STATE_CONNECTED;
            members = channel.getView().getMembers();
        } catch (Exception ex) {
            Logger.getLogger(JGroupsCommService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void disconnect() {
        channel.close();
        state = CommService.STATE_DISCONNECTED;
    }
    
    @Override
    public void send(String message) {
        Message msg = new Message(null, null, message);
        try {
            channel.send(msg);
        } catch (Exception ex) {
            Logger.getLogger(JGroupsCommService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void send(String address, String message) {
        Address memberAddress = null;
        
        for (int i = 0; i < channel.getView().getMembers().size(); i++) {
            if (channel.getView().getMembers().get(i).toString().equals(address)) {
                memberAddress = channel.getView().getMembers().get(i);
            }
        }
        
        if (memberAddress != null) {
            Message msg = new Message(memberAddress, null, message);
            
            try {
                channel.send(memberAddress, msg);
            } catch (Exception ex) {
                Logger.getLogger(JGroupsCommService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Logger.getLogger(JGroupsCommService.class.getName()).log(Level.SEVERE, null, "Member not found");
        }
    }

    public void viewAccepted(View new_view) {
        System.out.println("** view: " + new_view);
        members = new_view.getMembers();
    }

    public void receive(Message msg) {
        CommMessage message = new CommMessage();
        String str = new String(msg.getBuffer());
        message.setBody(str);
        message.setType(CommMessage.MESSAGE_READ);
        handler.handleResponse(message);
    }

    @Override
    public String[] getDeviceList() {
        String[] deviceList = new String[channel.getView().getMembers().size()];
        for (int i = 0; i < deviceList.length; i++) {
            deviceList[i] = channel.getView().getMembers().get(i).toString();
        }        
        return deviceList;
    }

    @Override
    public String getLocalAddress() {
        return channel.getAddressAsString();
    }
}
