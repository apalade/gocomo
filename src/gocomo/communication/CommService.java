package gocomo.communication;

/**
 * This service takes care of communication between the current node and other 
 * nodes
 * 
 * It should technically be an interface where a user can attached different 
 * types of communication adapters (Bluetooth, WIFI)
 * 
 * @author Andrei
 */
public interface CommService {

    public static final int STATE_NONE = 0; // we're doing nothing   
    public static final int STATE_LISTEN = 1; // now listening for incoming
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing
    public static final int STATE_CONNECTED = 3; // now connected to a remote device
    public static final int STATE_DISCONNECTED = 4; // we're disconneced now
    
    public int getState();
    
    /**
     * Broadcasts a given message to all devices
     * 
     * @param message 
     */
    public void send(String message);
    
    /**
     * Sends a message only to the given address (unicast)
     * 
     * @param address Destination address
     * @param message The content message
     */
    public void send(String address, String message);
    
    public void connect();
    
    public void disconnect();
    
    public String getLocalAddress();
    
    /** @return A list of strings where each is the address of a device */
    public String[] getDeviceList();
}
