package gocomo.communication;

/**
 * Message class (possibly interface later on to be used by the CommService and CommAdapter
 * 
 * @author Andrei
 */
public class CommMessage {
    
    
    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    private int type;
    private String tag;
    private String message;
    private long epoch = 0;
    private String sender;
    
    public CommMessage() {
        
    }
    
    public CommMessage(String sender, String message) {
        this.sender = sender;
        this.message = message;
    }

    public CommMessage(String tag, String message, long time) {
        this.tag = tag;
        this.message = message;
        epoch = time;
    }
    
    public int getType() {
        return type;
    }

    public String getTag() {
        return tag;
    }

    public String getBody() {
        return message;
    }

    public long getEpochTime() {
        return epoch;
    }
    
    public void setBody(String message) {
        this.message = message;
    }
    
    public void setType(int type) {
        this.type = type;
    } 

    @Override
    public String toString() {
        return tag + " " + epoch + " " + message;
    }
}
