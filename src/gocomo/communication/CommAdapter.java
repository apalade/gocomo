package gocomo.communication;

/**
 *
 * @author Andrei
 */
public class CommAdapter {
    
    
    private static CommAdapter instance = new CommAdapter();
    
    private CommAdapter() {
        enable();
    }
    
    // TODO: Implement the communication adapter
    public static CommAdapter getDefaultAdapter() {
        if (instance == null) {
            instance = new CommAdapter();
        }
        return instance;
    }
    
    // Possibly the mac address
    public String getAddress(){
        return "";
    }
    
    public boolean isEnabled() {
        return true;
    }
    
    // Throw some exception here in case we're unable to enable adapter
    public void enable() {
        
    }

    public CommDevice getRemoteDevice(String address) {
        return new CommDevice();
    }

}
