package gocomo.communication;

/**
 * Response Handler interface on how the message needs to handled once received
 * 
 * @author Andrei
 */
public interface ResponseHandler {
 
    public void handleResponse(CommMessage message);
}
