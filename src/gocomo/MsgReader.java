package gocomo;

import gocomo.messages.GocomoMessage;

/**
 * Created by NanxiChen on 8/23/2015.
 */
public class MsgReader {
    /*GocomoMessage gMsg;
    String gMsgString;
    public MsgReader(GocomoMessage gMsg) {
        this.gMsg = gMsg;
        this.gMsgString =gMsg.getStringMsg();
    }*/
    public static String getType(GocomoMessage gMsg){
        String type="";
        String gMsgString =gMsg.getStringMsg();
        if (gMsgString.indexOf("<type>")!=-1)type=gMsgString.substring(gMsgString.indexOf("<type>")+6, gMsgString.indexOf("</type>"));
        return type;
    }
    public static String getInput(GocomoMessage gMsg){
        String input="";
        String gMsgString =gMsg.getStringMsg();
        if (gMsgString.indexOf("<input>")!=-1)input=gMsgString.substring(gMsgString.indexOf("<input>")+7, gMsgString.indexOf("</input>"));
        return input;
    }
    public static String getOutput(GocomoMessage gMsg){
        String output="";
        String gMsgString =gMsg.getStringMsg();
        if (gMsgString.indexOf("<output>")!=-1)output=gMsgString.substring(gMsgString.indexOf("<output>")+8, gMsgString.indexOf("</output>"));
        return output;
    }
    public static String getQos(GocomoMessage gMsg){
        String qos="";
        String gMsgString =gMsg.getStringMsg();
        if (gMsgString.indexOf("<qos>")!=-1)qos=gMsgString.substring(gMsgString.indexOf("<qos>")+5, gMsgString.indexOf("</qos>"));
        return qos;
    }
    public static String getHeu(GocomoMessage gMsg){
        String heu="";
        String gMsgString =gMsg.getStringMsg();
        if (gMsgString.indexOf("<heu>")!=-1)heu=gMsgString.substring(gMsgString.indexOf("<heu>")+5, gMsgString.indexOf("</heu>"));
        return heu;
    }
    public static String getCache(GocomoMessage gMsg){
        String cache="";
        String gMsgString =gMsg.getStringMsg();
        if (gMsgString.indexOf("<cache>")!=-1)cache=gMsgString.substring(gMsgString.indexOf("<cache>")+7, gMsgString.indexOf("</cache>"));
        return cache;
    }
    public static String getSender(GocomoMessage gMsg){
        String sender="";
        String gMsgString =gMsg.getStringMsg();
        if (gMsgString.indexOf("<sender>")!=-1)sender=gMsgString.substring(gMsgString.indexOf("<sender>")+8, gMsgString.indexOf("</sender>"));
        return sender;
    }
    public static String getClient(GocomoMessage gMsg){
        String client="";
        String gMsgString =gMsg.getStringMsg();
        if (gMsgString.indexOf("<client>")!=-1)client=gMsgString.substring(gMsgString.indexOf("<client>")+8, gMsgString.indexOf("</client>"));
        return client;
    }
    public static String getId(GocomoMessage gMsg){
        String id="";
        String gMsgString =gMsg.getStringMsg();
        if (gMsgString.indexOf("<id>")!=-1)id=gMsgString.substring(gMsgString.indexOf("<id>")+4, gMsgString.indexOf("</id>"));
        return id;
    }
    public static String getWaypoint(GocomoMessage gMsg){
        String point="";
        String gMsgString =gMsg.getStringMsg();
        if (gMsgString.indexOf("<waypoint>")!=-1)point=gMsgString.substring(gMsgString.indexOf("<waypoint>")+10, gMsgString.indexOf("</waypoint>"));
        return point;
    }
    public static String getGuidepost(GocomoMessage gMsg){
        String guide="";
        String gMsgString =gMsg.getStringMsg();
        if (gMsgString.indexOf("<guide>")!=-1)guide=gMsgString.substring(gMsgString.indexOf("<guide>")+7, gMsgString.indexOf("</guide>"));
        return guide;
    }

}
