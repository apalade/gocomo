package gocomo.tests;

import gocomo.*;
import java.util.logging.Logger;

/**
 * @author Andrei
 */
public class GocomoTestClient3 {

    private final static Logger LOGGER = Logger.getLogger(GocomoTestClient3.class.getName());

    public static void main(String[] args) {

        // usage java -jar Gocomo.jar 
        // Two tasks: 
        // Register services task
        // Make request task
        System.out.println("Available tasks: ");
        //   System.out.println("1. Register services ");
        System.out.println("1. Make request ");


        final GocomoMain gocomo = new GocomoMain();
        
//        if (args.length != 1) {
//            System.out.println("Incorrect number of parameters");
//        } else {
//            gocomo.registerService(args[0]);
//        }
        
        gocomo.registerService("b.e.5");
        
    //      gocomo.request("c.e.5");
        
    }
}
