package gocomo;

import java.util.List;

/**
 * Created by NanxiChen on 8/23/2015.
 */
public class Direction {

    private int id;
    private List<String> postConditionList;
    private List<String> waypointList;
    private String postCondition;
    private String waypoint;
    private double qos;

    public Direction() {

    }

    public Direction(int id, String postCondition, String waypoint, double qos) {
        this.waypoint = waypoint;
        this.postCondition = postCondition;
        this.id = id;
        //this.waypoint.add(waypoint);
        //this.postCondition.add(postCondition);
        this.qos = qos;
    }

    public void addPost(String post) {
        postCondition = postCondition + "+" + post;
    }

    public void addWaypoint(String way) {
        waypoint = waypoint + "+" + way;
    }

    public void updateQos(double q) {
        qos = q;
    }

    public String getPostCondition() {
        return postCondition;
    }

    public String getWaypoint() {
        return waypoint;
    }

    public double getQos() {
        return qos;
    }

    public int getId() {
        return id;
    }
}
