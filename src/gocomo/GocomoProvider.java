package gocomo;

import gocomo.messages.Builders;
import gocomo.messages.CompleteMessageBuilder;
import gocomo.messages.DiscoveryMessage;
import gocomo.messages.ExecutionTokenBuilder;
import gocomo.messages.GocomoMessage;
import gocomo.messages.InvocationTokenBulider;
import gocomo.messages.MessageGen;
import gocomo.messages.ResultMessageBuilder;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by NanxiChen on 8/23/2015.
 */
public class GocomoProvider {
      
    Map<String, String> preferences = new HashMap<>();
    
    public static final String DISCOVERY_REQUEST = "dscv";
    public static final String BASIC_REQUEST = "req";
    public static final String EXECUTION_REQUEST = "exet";
    public static final String INVOCATION_REQUEST = "ivct";

    public GocomoProvider(final Map<String, String> pref){
        this.preferences = pref;
   
        preferences.put("cplt_out", null);
        preferences.put("out_pool", null);
        //this.MsgReader=MsgReader;
       // this.actionMap = new HashMap<>();
    }
    
    // Performs discovery
    public void performDSCV(GocomoMessage msg) {
        String outR =MsgReader.getOutput(msg);
        String outS= preferences.get("s_out");
        String inR =MsgReader.getInput(msg);
        String inS= preferences.get("s_in");
        //get history
        String oldCache=null;
        String newCache=MsgReader.getCache(msg);
        GocomoMessage historyMsg;

        if(preferences.get("dscv_out")!=null) {
            historyMsg = new GocomoMessage();
            historyMsg.setNewMsg(preferences.get("dscv_out"));
            // oldCache = MsgReader.getCache(historyMsg);
        }

        if(preferences.get("cache_unsolved")!=null) {
            oldCache = preferences.get("cache_unsolved");
        }
        String adaptEvent=LogicChecker.genLogic(outS,outR,oldCache, newCache);
        //TODO logicController needs to return a value that indicates

        //SharedPreferences.Editor editor = sharedPref.edit();
        if(adaptEvent.equals("unusable")){
            preferences.put("out_pool_type", "done");
            //editor.commit();
        }
        else {
            GuidepostManager gManager = new GuidepostManager(preferences);
            gManager.update(adaptEvent, msg);

            if (LogicChecker.isFinished(inS, inR)) {
                //Generate a complete message
                MessageGen msgGen = new MessageGen();
                Builders completeMsgBuilder = new CompleteMessageBuilder();
                msgGen.setMsgBuilder(completeMsgBuilder);
                msgGen.constructMessage(1, MsgReader.getClient(msg), "-|-|" + preferences.get("myaddress") + "|44", "-|-");
                String messageString = msgGen.getMessageString();

                preferences.put("cplt_out", messageString);
                preferences.put("out_pool", messageString);
                preferences.put("out_pool_address", MsgReader.getClient(msg));
                preferences.put("out_pool_type", "cplt");
            } else {

                String msgContent = MsgReader.getInput(msg) + "|" + inS + "|" + preferences.get("myaddress") + "|44";
                MessageGen msgGen = new MessageGen();
                Builders dscvMsgBuilder = new DiscoveryMessage();
                msgGen.setMsgBuilder(dscvMsgBuilder);
                msgGen.constructMessage(1, MsgReader.getClient(msg), msgContent, "-|-");
                String messageString = msgGen.getMessageString();

                preferences.put("removeSender", MsgReader.getSender(msg));
                preferences.put("dscv_out", messageString);
                preferences.put("out_pool", messageString);
                preferences.put("out_pool_type", "dscv");
            }
        }
    }
    
    public void performREQ(GocomoMessage msg) {
        String outR =MsgReader.getOutput(msg);
        String outS= preferences.get("s_out");
        String inR =MsgReader.getInput(msg);
        String inS= preferences.get("s_in");
        //get history
        String oldCache=null;
        String newCache=MsgReader.getCache(msg);
        GocomoMessage historyMsg;

        if(preferences.get("dscv_out")!=null) {
            historyMsg = new GocomoMessage();
            historyMsg.setNewMsg(preferences.get("dscv_out"));
            // oldCache = MsgReader.getCache(historyMsg);
        }
        if(preferences.get("cache_unsolved")!=null) {
            oldCache = preferences.get("cache_unsolved");
        }
        String adaptEvent=LogicChecker.genLogic(outS,outR,oldCache, newCache);
        //TODO logicController needs to return a value that indicates

        if(adaptEvent.equals("unusable")){
            preferences.put("out_pool_type", "done");
        }
        else {
            GuidepostManager gManager = new GuidepostManager(preferences);
            gManager.update(adaptEvent, msg);

            if (LogicChecker.isFinished(inS, inR)) {
                //Generate a complete message
                MessageGen msgGen = new MessageGen();
                Builders completeMsgBuilder = new CompleteMessageBuilder();
                msgGen.setMsgBuilder(completeMsgBuilder);
                msgGen.constructMessage(1, MsgReader.getClient(msg), "-|-|" + preferences.get("myaddress") + "|44", "-|-");
                String messageString = msgGen.getMessageString();

                preferences.put("cplt_out", messageString);
                preferences.put("out_pool", messageString);
                preferences.put("out_pool_address", MsgReader.getClient(msg));
                preferences.put("out_pool_type", "cplt");
            } else {

                String msgContent = MsgReader.getInput(msg) + "|" + inS + "|" + preferences.get("myaddress") + "|44";
                MessageGen msgGen = new MessageGen();
                Builders dscvMsgBuilder = new DiscoveryMessage();
                msgGen.setMsgBuilder(dscvMsgBuilder);
                msgGen.constructMessage(1, MsgReader.getClient(msg), msgContent, "-|-");
                String messageString = msgGen.getMessageString();

                preferences.put("removeSender", MsgReader.getSender(msg));
                preferences.put("dscv_out", messageString);
                preferences.put("out_pool", messageString);
                preferences.put("out_pool_type", "dscv");
            }
        }
    }
      
    public void performEXET(GocomoMessage msg) {
        //String inData= sharedPref.getString("d_in", null);

        //get next service provider's address
        String nextProvider= null;
        try {
            nextProvider = GuidepostManager.getDirection(MsgReader.getId(msg), preferences);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String messageString=null;
        String inData=preferences.get("s_out").toUpperCase();
        if (!nextProvider.equals(null)) {
            
            if(nextProvider.equals(MsgReader.getClient(msg))){
                //Generate a invocation message
                MessageGen msgGen = new MessageGen();
                Builders exeBuilder = new ResultMessageBuilder();
                msgGen.setMsgBuilder(exeBuilder);
                msgGen.constructMessage(1, MsgReader.getClient(msg), inData, "-|-");
                messageString=msgGen.getMessageString();
            
            }else {
                //Generate a invocation message
                MessageGen msgGen = new MessageGen();
                Builders exeBuilder = new ExecutionTokenBuilder();
                msgGen.setMsgBuilder(exeBuilder);
                msgGen.constructMessage(1, MsgReader.getClient(msg), inData, "-|-");
                messageString=msgGen.getMessageString();
            }
            
            preferences.put("cplt_out", messageString);
            preferences.put("out_pool", messageString);
            preferences.put("out_pool_address", nextProvider);
            preferences.put("out_pool_type", "exet");
            
        }else{

        }
    }
     
    // Performs invocation
    public void performIVCT(GocomoMessage msg) {
      //  String inData= preferences.get("d_in");

        
        System.out.println("INVOCATION MESSAGE");        
        
        //Generate a invocation message
        MessageGen msgGen = new MessageGen();
        Builders invocationBuilder = new InvocationTokenBulider();
        msgGen.setMsgBuilder(invocationBuilder);
        msgGen.constructMessage(1,MsgReader.getClient(msg), preferences.get("d_in"),"-|-");
        String messageString=msgGen.getMessageString();
        
        //get next service provider's address
        String nextProvider= null;
        try {
            nextProvider = GuidepostManager.getDirection(MsgReader.getId(msg), preferences);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        preferences.put("cplt_out", messageString);
        preferences.put("out_pool", messageString);
        preferences.put("out_pool_address", nextProvider);
        preferences.put("out_pool_type", "ivct");
    }
    
    
    public void active(GocomoMessage msg, String msgType){
        switch(msgType) {
            case DISCOVERY_REQUEST:
                performDSCV(msg);
                break;
            case BASIC_REQUEST:
                performREQ(msg);
                break;
            case EXECUTION_REQUEST:
                performEXET(msg);
                break;
            case INVOCATION_REQUEST:
                performIVCT(msg);
                break;
            default:
                break;
        }
        
    }
}
