package gocomo;

import java.util.prefs.Preferences;


import gocomo.messages.GocomoMessage;
import gocomo.utils.JsonUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by NanxiChen on 8/23/2015.
 */
public class GuidepostManager {

    HashMap<String, GuidepostAction> actionMap = new HashMap<>();
    Preferences sharedPref;
    
    Map<String, String> preferences = new HashMap<>();
    
    Guidepost guidepostUpdate;
    JsonUtil jsonUtil=new JsonUtil();
   
    
    public GuidepostManager(final Map<String, String> preferences){
        this.preferences = preferences;
        actionMap.put("add", new GuidepostAction() {
            @Override
            public void perform(GocomoMessage msg){
                int guidepostCount;
                List<Direction> guidepost;
                String guidepostRecord;
                if(preferences.get("guide_json_"+MsgReader.getId(msg)) != null){
             //   if(sharedPref.keys().length != 0 ){
                    guidepostRecord=preferences.get("guide_json_"+MsgReader.getId(msg));
                    guidepost = jsonUtil.toGuidepost(guidepostRecord);
                    guidepostCount=guidepost.size();
                }else{
                    guidepost=new ArrayList<>();
                    guidepostCount=0;
                }
                guidepostUpdate=new Guidepost(guidepost);
                double qos=1;
                guidepostUpdate.addDirection(MsgReader.getSender(msg), MsgReader.getWaypoint(msg), qos);
                guidepostCount=guidepostUpdate.getGuidepost().size();
                guidepost=guidepostUpdate.getGuidepost();
                String guidepostM=jsonUtil.toJSonGuidepost(guidepost);
                preferences.put("guide_json_"+MsgReader.getId(msg), guidepostM);
                
            }
        });

        // the new direction is point to a waypoint
        actionMap.put("addjoin", new GuidepostAction() {
            @Override
            public void perform(GocomoMessage msg){
                List<Direction> guidepost;
                int guidepostCount;
                String guidepostRecord;
                if(preferences.get("guide_json_"+MsgReader.getId(msg)) != null){
                    guidepostRecord=preferences.get("guide_json_"+MsgReader.getId(msg));
                    guidepost = jsonUtil.toGuidepost(guidepostRecord);
                    guidepostCount=guidepost.size();
                }else{
                    guidepost=new ArrayList<>();
                    guidepostCount=0;
                }
                double qos=1;
                String waypoint=MsgReader.getWaypoint(msg)+"+"+MsgReader.getSender(msg);
                guidepostUpdate=new Guidepost(guidepost);
                guidepostUpdate.addDirection(MsgReader.getSender(msg), waypoint, qos);
                guidepostCount=guidepostUpdate.getGuidepost().size();
                String guidepostM=jsonUtil.toJSonGuidepost(guidepost);
                preferences.put("guide_json_"+MsgReader.getId(msg), guidepostM);
                
            }
        });
        //add a split branch to all direction
        actionMap.put("addsplit",   new GuidepostAction() {
            @Override
            public void perform(GocomoMessage msg){
                List<Direction> guidepost;
                int guidepostCount;
                String guidepostRecord;
                if(preferences.get("guide_json_"+MsgReader.getId(msg)) != null){
                    guidepostRecord=preferences.get("guide_json_" + MsgReader.getId(msg));
                    guidepost = jsonUtil.toGuidepost(guidepostRecord);
                    //guidepostCount=guidepost.size();
                }else{
                    guidepost=new ArrayList<>();
                    //guidepostCount=0;
                }
                double qos=1;
                guidepostUpdate=new Guidepost(guidepost);
                guidepostUpdate.updateGuidepost(MsgReader.getSender(msg), qos);
                String guidepostM=jsonUtil.toJSonGuidepost(guidepost);
                preferences.put("guide_json_"+MsgReader.getId(msg), guidepostM);
            }
        });
        actionMap.put("adapt",   new GuidepostAction() {
            @Override
            public void perform(GocomoMessage msg){
                List<Direction> guidepost;
                int guidepostCount;
                String guidepostRecord;
                if(preferences.get("guide_json_"+MsgReader.getId(msg)) != null){
                    guidepostRecord=preferences.get("guide_json_" + MsgReader.getId(msg));
                    guidepost = jsonUtil.toGuidepost(guidepostRecord);
                    //guidepostCount=guidepost.size();
                }else{
                    guidepost=new ArrayList<>();
                    //guidepostCount=0;
                }
                double qos=1;
                String waypoint=MsgReader.getWaypoint(msg)+MsgReader.getSender(msg);
                guidepostUpdate = new Guidepost(guidepost);
                guidepostUpdate.remove(MsgReader.getSender(msg), MsgReader.getWaypoint(msg), qos);
                String guidepostM=jsonUtil.toJSonGuidepost(guidepost);
                preferences.put(MsgReader.getId(msg), guidepostM);
                
            }
        });

        //String dummyInput = "input2";
        //actionMap.get(dummyInput).perform(dummyInput);
    }
    
    public void update(String event,  GocomoMessage msg){

        actionMap.get(event).perform(msg);
    }
   
    public static String getDirection(String id, Preferences sharedPref) throws JSONException {
        List<Direction> guidepost=new ArrayList<Direction>();
        int guidepostCount;
        JsonUtil jsonUtil=new JsonUtil();
        String guidepostRecord;
        if(sharedPref.get("guide_json_"+id, null) != null){
            guidepostRecord=sharedPref.get("guide_json_" + id, null);
            guidepost = jsonUtil.toGuidepost(guidepostRecord);
            //guidepostCount=guidepost.size();
        }else{
            //TODO return failure
        }
        Guidepost localGuidepost=new Guidepost(guidepost);
        String address=localGuidepost.getBest();
        return address;
    }

    public static String getDirection(String id, Map<String, String> preferences) throws JSONException {
        List<Direction> guidepost=new ArrayList<Direction>();
        int guidepostCount;
        JsonUtil jsonUtil=new JsonUtil();
        String guidepostRecord;
        if(preferences.get("guide_json_"+id) != null){
            guidepostRecord=preferences.get("guide_json_" + id);
            guidepost = jsonUtil.toGuidepost(guidepostRecord);
            //guidepostCount=guidepost.size();
        }else{
            //TODO return failure
        }
        Guidepost localGuidepost=new Guidepost(guidepost);
        String address=localGuidepost.getBest();
        return address;
    }
}

interface GuidepostAction {
    void perform(GocomoMessage msg);
}