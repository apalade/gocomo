package gocomo;

import gocomo.communication.CommAdapter;
import gocomo.communication.CommService;
import gocomo.communication.CommMessage;

import gocomo.messages.Builders;
import gocomo.messages.CompositionRequest;
import gocomo.messages.GocomoMessage;
import gocomo.messages.MessageGen;
import gocomo.communication.ResponseHandler;
import gocomo.communication.vendors.JGroupsCommService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GocomoMain implements ResponseHandler{

        private final static Logger LOGGER = Logger.getLogger(GocomoMain.class.getName());

        // For debugging purposes
        private static final boolean D = true;


        // Name of the connected device
        private String mConnectedDeviceName = null;

        // Local Bluetooth adapter
        private CommAdapter mCommAdapter = null;

        // Member object for the chat services
        private CommService mChatService = null;

        //shared preferences
        Map<String, String> preferences = new HashMap<>();
        
        public GocomoMain() {
            if (D) LOGGER.log(Level.INFO, "+++ ON CREATE +++");

            mCommAdapter = CommAdapter.getDefaultAdapter();

            if (mCommAdapter != null) {
                preferences.put("logStatus", "creation time:");
                preferences.put("myaddress", mCommAdapter.getAddress());
            } else {
                LOGGER.log(Level.SEVERE, "Communication adapter is not available");
                return;
            }

            // If BT is not on, request that it be enabled.
            if (!mCommAdapter.isEnabled()) {
                mCommAdapter.enable();
            } else {
                // Otherwise, setup the chat session
                if (mChatService == null) {
                    if (D) LOGGER.log(Level.INFO, "setupChat()");
                    
                    // Initialize the array adapter for the conversation thread
                    mChatService = new JGroupsCommService(this);
                }
            }
            
            preferences.put("myaddress", mChatService.getLocalAddress());
            preferences.put("removeSender", mChatService.getLocalAddress());
        }

        // Retrieves and parses the user request and broadcasts the request
        // Format input.output.qos
        public void request(String message) {

            // Parse request
            String clientAddress = preferences.get("myaddress");
            String[] messageIn = message.split("[.]");
            String messageString = messageIn[0] + "|" + messageIn[1] + "|" + clientAddress + "|" + messageIn[2];
            
            // Build request message
            MessageGen msgGen = new MessageGen();
            Builders dscvMsgBuilder = new CompositionRequest();
            msgGen.setMsgBuilder(dscvMsgBuilder);
            msgGen.constructMessage(1, clientAddress, messageString);
            messageString = msgGen.getMessageString();
            
            preferences.put("out_req", messageString);
            preferences.put("d_in", "input");
            preferences.put("logStatus", "message generation time=");
                      
            String[] multicastList = mChatService.getDeviceList();
            
            if (messageString == null) LOGGER.log(Level.WARNING, "No requirements provided");
            else if(multicastList == null || multicastList.length == 0) LOGGER.log(Level.WARNING, "No multicast list available");
            else {
                sendService(messageString);
            }
            LOGGER.log(Level.INFO, "A composition request has been generated, "
                    + "requesting for: {0}; and providing: {1} as input.", new Object[]{messageIn[1], messageIn[0]});
        }
        
        // Send a message through a connected channel
        public void sendService(String outMsg) {
            String[] deviceList = mChatService.getDeviceList();
            
            if (deviceList == null || deviceList.length == 0) {
                LOGGER.log(Level.WARNING, "Please discover an available network");
            } else {
                List<String> list = new ArrayList<>(Arrays.asList(deviceList));
                String senderAddress = preferences.get("removeSender");
                list.remove(senderAddress);
                deviceList = list.toArray(new String[0]);

                for (String deviceAddress: deviceList) {
                    sendMessage(deviceAddress, outMsg);
                }
            }
        }
        
        private void sendMessage(String dest, String message) {
            // Check that we're actually connected before trying anything
            if (mChatService.getState() != CommService.STATE_CONNECTED) return;

            // Check that there's actually something to send
            if (message.length() > 0) {
                mChatService.send(dest, message);
            }
        }

        // Forwards input
        private void exeService(final String outMsg, final String dAddress) {
            // if geting a multicast address, do multicast
            String[] multicastListL = dAddress.split("[+]");

            for (String deviceAddress: multicastListL) {
                sendMessage(deviceAddress, outMsg);
            }
        }

        private void cpltService(final String outMsg, final String addresses) {
            // if geting a multicast address, do multicast
            String[] multicastList = addresses.split("[+]");

            for (final String deviceAddress: multicastList) {
                sendMessage(deviceAddress, outMsg);
            }
        }

        public void registerService(String serviceDescription) {
            String[] messageIn = serviceDescription.split("[.]");

            preferences.put("s_in", messageIn[0]);
            preferences.put("s_out", messageIn[1]);

            LOGGER.log(Level.INFO, "Local service registered: Input-{0}; Output-{1}.", new Object[]{messageIn[0], messageIn[1]});
        }

        private void performGoCoMo(String x) {
            //log start time
            GocomoMessage msgIn = new GocomoMessage();
            msgIn.setNewMsg(x);
            String action = MsgReader.getType(msgIn);

            // based on the type of received message, choose an action and role to perform
            // cplt - complete message
            // rslt - result message
            if (action.equals("cplt") || action.equals("rslt")) {
                GocomoClient gocomoClient = new GocomoClient(preferences);
                gocomoClient.active(msgIn, action);
                if (action.equals("cplt")) {
                    preferences.put("logStatus", "client's preparation time for execution:");
                } else {
                    preferences.put("logStatus", "client gets execution result:");
              //      long timeCost = System.currentTimeMillis() - Long.valueOf(preferences.get("ClientStart"));

                    LOGGER.log(Level.INFO, "Composition result:  {0}", new Object[]{MsgReader.getInput(msgIn)}
                    );
                }
            } else {
                if (!MsgReader.getClient(msgIn).equals(preferences.get("myaddress"))) {
                    GocomoProvider gProvider = new GocomoProvider(preferences);
                    gProvider.active(msgIn, action);
                    if (action.equals("dscv") || action.equals("req")) {
                        preferences.put("logStatus", "provider planning:");
                    } else {
                        preferences.put("logStatus", "provider execution:");
                    }
                } else {
                    preferences.put("out_pool_type", "done");
                    preferences.put("logStatus", "provider mismatch:");
                }
            }

            //exam the out message and send them out
            switch (preferences.get("out_pool_type")) {
                case "dscv":
                    LOGGER.log(Level.INFO, "Discovering services");
                    sendService(preferences.get("out_pool"));
                    break;

                case "cplt":
                    LOGGER.log(Level.INFO, "Returning discovery result...");
                    cpltService(preferences.get("out_pool"), preferences.get("out_pool_address"));
                    break;
                    
                case "done":
                    //do nothing, execution finished
                    break;    
                    
                default:
                    LOGGER.log(Level.INFO, "Invoking {0}", preferences.get("out_pool_address"));
                    exeService(preferences.get("out_pool"), preferences.get("out_pool_address"));
                    break;
            }
        }

    @Override
    public void handleResponse(CommMessage message) {
        switch (message.getType()) {
            case CommMessage.MESSAGE_STATE_CHANGE:

                if (D) LOGGER.log(Level.INFO, "MESSAGE_STATE_CHANGE: {0}", message.getBody());
                
                // Need to check if the body of the message has the state
                int state = Integer.parseInt(message.getBody());

                switch (state) {

                    case CommService.STATE_CONNECTED:
                        LOGGER.log(Level.INFO, "Connected to {0}", mConnectedDeviceName);
                        break;

                    case CommService.STATE_CONNECTING:
                    case CommService.STATE_LISTEN:
                    case CommService.STATE_NONE:
                    default:
                        break;
                }

            case CommMessage.MESSAGE_WRITE:
                if (D) LOGGER.log(Level.INFO, "MESSAGE_WRITE: {0}", message.getBody());
                break;

            case CommMessage.MESSAGE_READ:
                if (D) LOGGER.log(Level.INFO, "MESSAGE_READ: {0}", message.getBody());
                performGoCoMo(message.getBody());
                break;

            case CommMessage.MESSAGE_DEVICE_NAME:
                if (D) LOGGER.log(Level.INFO, "MESSAGE_DEVICE_NAME: {0}", message.getBody());
                
                // save the connected device's name
                mConnectedDeviceName = message.getBody();
                break;

            case CommMessage.MESSAGE_TOAST:
                if (D) LOGGER.log(Level.INFO, "MESSAGE_TOAST: {0}", message.getBody());
                break;
        }
    }
}
