package gocomo;

import java.util.prefs.Preferences;

import gocomo.messages.Builders;
import gocomo.messages.ExecutionTokenBuilder;
import gocomo.messages.GocomoMessage;
import gocomo.messages.MessageGen;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by NanxiChen on 8/23/2015.
 */
public class GocomoClient {
    Map<String, ClientAction> actionMap = new HashMap<>();
    Preferences sharedPref;
    
    Map<String, String> preferences = new HashMap<>();
    
    //MsgReader MsgReader; 
    public GocomoClient(final Map<String, String> preferences){
        this.preferences = preferences;
        preferences.put("cplt_out", null);
        preferences.put("out_pool", null);
        
        actionMap.put("cplt",   new ClientAction() {
            @Override
            public void perform(GocomoMessage msg){

                String inData = preferences.get("d_in");

                //Generate a invocation message
                MessageGen msgGen = new MessageGen();
                Builders executionBuilder = new ExecutionTokenBuilder();
                msgGen.setMsgBuilder(executionBuilder);
                msgGen.constructMessage(1,MsgReader.getClient(msg), inData,"-|-");
                String messageString=msgGen.getMessageString();
                String nextProvider= MsgReader.getSender(msg);

                //Preferences.Editor editor = sharedPref.edit();
                preferences.put("cplt_out", messageString);
                preferences.put("out_pool", messageString);
                preferences.put("out_pool_address", nextProvider);
                preferences.put("out_pool_type", "exet");
               // editor.commit();

            }
        });
        actionMap.put("rslt",   new ClientAction() {
            @Override
            public void perform(GocomoMessage msg){
                //Preferences.Editor editor = sharedPref.edit();
                //editor.clear();
                preferences.put("composition_result", MsgReader.getOutput(msg));
                preferences.put("out_pool_type", "done");
             //   editor.commit();

            }
        });
    }
    
    
    public void active(GocomoMessage msg, String msgType){
        actionMap.get(msgType).perform(msg);
    }
}
interface ClientAction {
    void perform(GocomoMessage msg);
}
