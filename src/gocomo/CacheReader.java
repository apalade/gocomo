package gocomo;

/**
 * Created by NanxiChen on 8/23/2015.
 */
public class CacheReader {
    // cache = <waypoint id> <matched output> <progress>
    // cache's update, in a message, all the new cache entity is insert to the begining of a cache set
    String[] cachSet;
    public static String  getNearestCache(String cachSet){
        String heu="";
        if (cachSet.contains("<c_waypoint>"))
            heu=cachSet.substring(cachSet.indexOf("<c_waypoint>"), cachSet.indexOf("</c_progress>")+12);
        return heu;
    }
    public static String  getWaypoint(String cachSet){
        String heu="";
        if (cachSet.contains("<c_waypoint>"))
            heu=cachSet.substring(cachSet.indexOf("<c_waypoint>")+12, cachSet.indexOf("</c_waypoint>"));
        return heu;

    }
    public static String  getSolvedOutput(String cachSet){
        String heu="";
        if (cachSet.contains("<c_matched>"))
            heu=cachSet.substring(cachSet.indexOf("<c_matched>")+11, cachSet.indexOf("</c_matched>"));
        return heu;
    }
    public static String  getProgress(String cachSet){
        String heu="";
        if (cachSet.contains("<c_progress>"))
            heu=cachSet.substring(cachSet.indexOf("<c_progress>")+12, cachSet.indexOf("</c_progress>"));
        return heu;
    }
}
