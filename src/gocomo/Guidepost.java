package gocomo;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by NanxiChen on 8/23/2015.
 */
public class Guidepost {
   
    List<Direction> guidepost;
     int guideposCount=0;
    
    String rId;
    //SharedPreferences shared;
    public Guidepost(List<Direction> guidepost ){
        if(guidepost.size()>0) {
            this.guidepost=guidepost;
            this.guideposCount = guidepost.size();
        }else{
            this.guideposCount=0;
            this.guidepost=new ArrayList<Direction>();
        }
    }

    public void addDirection(String post,String waypoint, double qos) {

        //gen a direction id
        int dId=guideposCount+1;
        guideposCount++;
        Direction d = new Direction(dId, post, waypoint, qos);
        guidepost.add(d);
    }
    public List<Direction> getGuidepost() {
      return guidepost;
    }


    public void updateGuidepost(String post, double qos) {
        //add one postcondition node to all the directions for request i (when they have the same waypoint)
        //List<Direction> guidepost = prepareGuidepost(rId);
        for (int i = 0; i < guidepost.size(); i++) {
            Direction d=guidepost.get(i);
            guidepost.remove(i);
            d.addPost(post);
            d.updateQos(qos);
            guidepost.add(d);
        }
    }
    public void remove(String post,String waypoint, double qos) {
        //remove all the directions in the guidepost and insert a new one
        List<Direction> guide=guidepost;
        guide.clear();
        guideposCount=1;
        Direction d = new Direction(1, post, waypoint, qos);
        guide.add(d);
    }
    public String getBest() {
        double temp_qos=0;
        String temp_address=null;
        for (int i = 0; i < guidepost.size(); i++) {
            Direction d=guidepost.get(i);
            double qos=d.getQos();
            if( qos >temp_qos){
                temp_qos=qos;
                temp_address=d.getPostCondition();
            }
        }
        return temp_address;
    }
}
