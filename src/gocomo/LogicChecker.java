package gocomo;

import gocomo.utils.Matchmaker;

/**
 * Created by NanxiChen on 8/23/2015.
 */
public class LogicChecker {

    
    private final static String UNUSABLE = "unusable";
    private final static String USABLE = "usable";
    private final static String PARTUSABLE = "partusable";
    
    public LogicChecker(){

    }
    public static String genLogic(String service, String req, String oldCache, String newCache){
        String event="unusable";
       // if(a.equals(b))
        String matching= Matchmaker.match(service, req);
        if("usable".equals(matching))
            event="add";
        else if("partusable".equals(matching)){
            //sharedPref.getString()
            if((oldCache!=null)||(newCache!="")){
                //TODO check if the caches can merge
                String newC=CacheReader.getNearestCache(newCache);
                if(cacheMatch(newC,oldCache)) {
                    String cacheUpdate=getCacheUpdateData(newC,oldCache);
                    event = "addSplit" +"<cacheUpdate>"+cacheUpdate;
                }
            }
        } 
        else {
            
        }
        return event;
    }
    public static boolean isFinished(String inService, String inReq){
        boolean event=false;
        //if any in the input in inReq satisfied by in Service
        String matching= Matchmaker.match(inService, inReq);
        if(!"unusable".equals(matching))
            event=true;
        return event;
    }
    private static boolean cacheMatch(String  newCache, String allUnsolvedCache){
        boolean event=false;
        String newC=CacheReader.getWaypoint(newCache);
        String oldC=CacheReader.getWaypoint(allUnsolvedCache);
        String newOut=CacheReader.getSolvedOutput(newCache);
        String oldOut=CacheReader.getSolvedOutput(allUnsolvedCache);
        String newP=CacheReader.getProgress(newCache);
        String oldP=CacheReader.getProgress(allUnsolvedCache);
        if((newC==oldC)&&(newOut!=oldOut)&&(Integer.parseInt(newP)+Integer.parseInt(oldP)==1)) {
            event = true;
        }
        return event;
    }
    private static String getCacheUpdateData(String  newCache, String allUnsolvedCache){
        String event="";
        //find matched cache, clear

        return event;
    }
}
