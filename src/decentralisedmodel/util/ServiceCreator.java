package decentralisedmodel.util;

import decentralisedmodel.model.QOS;
import decentralisedmodel.model.Service;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Andrei-PC
 */
public class ServiceCreator {
    
    public static final String RESPONSE_TIME = "RT";
    public static final String ACCURACY = "accuracy";
    public static final String THROUGHPUT = "TP";
    public static final String PRICE = "PRICE";
    
    
    public static Map<String, Service> createServices(String serviceClass, String[] rtQOS, String[] acQOS) {
        Map<String, Service> map = new HashMap<>();
        for (int i = 0; i < rtQOS.length; i++) {
            String uuid = UUID.randomUUID().toString();
            Map<String, QOS> qos = new HashMap<>();
            qos.put(RESPONSE_TIME, new QOS(RESPONSE_TIME, Double.parseDouble(rtQOS[i]), false));
            qos.put(ACCURACY, new QOS(ACCURACY, Double.parseDouble(acQOS[i]), true));
            map.put(uuid, new Service(uuid, qos, serviceClass));
        }
        return map;
    }
    
    public static void main(String[] args) {
        
        String[] rt = FileParser.getValues(10, FileParser.RESPONSE_TIME);
        String[] acc = FileParser.getValues(10, FileParser.ACCURACY);
        Map<String, Service> serviceMap = ServiceCreator.createServices("TOPK", rt, acc);
        
        for(Map.Entry<String, Service> entry: serviceMap.entrySet()) {
            System.out.println(entry.getValue());
        }
        
        rt = FileParser.getValues(5000, 10, FileParser.RESPONSE_TIME);
        acc = FileParser.getValues(20, 10, FileParser.ACCURACY);
        serviceMap = ServiceCreator.createServices("TOPK", rt, acc);
        
        for(Map.Entry<String, Service> entry: serviceMap.entrySet()) {
            System.out.println(entry.getValue());
        }
    }
}
