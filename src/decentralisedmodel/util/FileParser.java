package decentralisedmodel.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andrei-PC
 */
public class FileParser {

    public static String[] filePaths = {"myData/ResponseTime.txt", "myData/Accuracy.txt"};
    
    public static int RESPONSE_TIME = 0;
    public static int ACCURACY = 1;
    
    public static String[] getValues(int numberOfValues, int type)  {

        FileInputStream fs= null;
        String[] qos = new String[numberOfValues];
        
        try {
            if (type == RESPONSE_TIME) {
                fs = new FileInputStream(filePaths[RESPONSE_TIME]);
            }
            else if (type == ACCURACY) {
                fs = new FileInputStream(filePaths[ACCURACY]);
            }
            else {
                // Default read time values
                fs = new FileInputStream(filePaths[RESPONSE_TIME]);
            }
            
            BufferedReader br = new BufferedReader(new InputStreamReader(fs));
            int number = 0;
            
            while (number < numberOfValues) {
                String line = br.readLine();
                String[] values = line.split("\t");
                
                for(int i = 0; i < values.length && number < numberOfValues; i++) {
                    qos[number] = values[i];
                    number++;
                }
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileParser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fs.close();
            } catch (IOException ex) {
                Logger.getLogger(FileParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return qos;
    }
    
     public static String[] getValues(int fromIndex, int numberOfValues, int type)  {

        FileInputStream fs= null;
        String[] qos = new String[numberOfValues];
        
        try {
            if (type == RESPONSE_TIME) {
                fs = new FileInputStream(filePaths[RESPONSE_TIME]);
            }
            else if (type == ACCURACY) {
                fs = new FileInputStream(filePaths[ACCURACY]);
            }
            else {
                // Default read time values
                fs = new FileInputStream(filePaths[RESPONSE_TIME]);
            }
            
            BufferedReader br = new BufferedReader(new InputStreamReader(fs));
            int number = 0;
            while (number < numberOfValues) {
                String line = br.readLine();
                String[] values = line.split("\t");
                int i = 0;
                for(; i < values.length && fromIndex > 0; i++) {
                    fromIndex--;
                }
                
                if (fromIndex == 0) {
                    for(; i < values.length && number < numberOfValues; i++) {
                        qos[number] = values[i];
                        number++;
                    }
                }
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileParser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fs.close();
            } catch (IOException ex) {
                Logger.getLogger(FileParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return qos;
    }

}
