package decentralisedmodel.util;

import decentralisedmodel.model.QOS;
import decentralisedmodel.model.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class implements the necessary tools to aggregate QoS information
 * 
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class QoSUtils {
    
    public static final String RESPONSE_TIME = "RT";
    public static final String ACCURACY = "accuracy";
    public static final String THROUGHPUT = "TP";
    public static final String PRICE = "PRICE";
    
    
    // Time and Price use sum
    // Availability and Reliability use product
    
    
    // aggregates a given qos property of a list of services
    // used to calculate the process level QoS
    public static double aggregate(Service[] serviceList, String qosName) {
        
        int sum = 0;
        
        for (Service service: serviceList) {
            
            if (qosName.equalsIgnoreCase(RESPONSE_TIME)) {
                sum += service.getQoSValue(RESPONSE_TIME);
            }
        }
        
        return sum;
    }
    
    
    public static boolean isEqual(List<QOS> list1, List<QOS> list2) {
        if (list1.size() != list2.size()) {
            return false;
        }
        else {
            for (int i = 0; i < list1.size(); i++) {
                if (list1.get(i).compareTo(list2.get(i)) != 0) {
                    return false;
                }
            }
        }
        return true;
    }
    
    
//    public static Map<String, List<QOS>> getQOSStack(List<Service> serviceList) {
//        Map<String, List<QOS>> qosStacks = new HashMap<>();
//
//        for (Service service : serviceList) {
//
//            // get the qos of each service in this class
//            List<QOS> qosLevel = service.getQOSLevel();
//
//            for (QOS qos : qosLevel) {
//
//                // add this to the map
//                if (qosStacks.containsKey(qos.getName())) {
//                    qosStacks.get(qos.getName()).add(qos);
//                } else {
//                    List<QOS> qosList = new ArrayList<>();
//                    qosList.add(qos);
//                    qosStacks.put(qos.getName(), qosList);
//                }
//            }
//        }
//
//        return qosStacks;
//    }
}
