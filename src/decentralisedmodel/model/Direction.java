package decentralisedmodel.model;

import java.util.List;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class Direction {
    
    private ServiceProvider serviceProvider;
    private List<QOS> qos;

    public ServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public List<QOS> getQos() {
        return qos;
    }

    public void setQos(List<QOS> qos) {
        this.qos = qos;
    }
}
