package decentralisedmodel.model;

import java.util.Objects;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class QOS implements Comparable<QOS>{
    
    private String name;
    private double value;
    private boolean positive;

    public QOS(String name, double value, boolean toMaximise) {
        this.name = name;
        this.positive = toMaximise;
        this.value = value;
    }
    
    public QOS(String name, double value) {
        this.name = name;
        this.positive = false;
        this.value = value;
    }
    
    /**
     * If we want to maximize this QOS value then we return 1
     * 
     * If we want to minimize this QOS value then we return -1
     * 
     * @return 
     */
    public double isPositiveInDigit() {
        return isPositive() ? 1 : -1;
    }    
    
    public boolean isPositive() {
        return positive;
    }
    
    public double getValue() {
        return value;
    }
    
    public void setValue(double v) {
        value = v;
    }
    
    public String getName() {
        return name;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final QOS other = (QOS) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(QOS o) {
        if (this.getValue() > o.getValue()) {
            return 1;
        }
        else if (this.getValue() == o.getValue()) {
            return 0;
        }
        else {
            return -1;
        }
    }
}
