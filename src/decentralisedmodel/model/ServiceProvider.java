package decentralisedmodel.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class ServiceProvider {
    
    private String id;
    private Map<String, Service> serviceMap;
    
    public ServiceProvider() {
        id = UUID.randomUUID().toString();
    }
    
    public ServiceProvider(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Service> getServices() {
        return serviceMap;
    }

    public void setServices(Map<String, Service> services) {
        this.serviceMap = services;
    }
    
    public List<Service> discoverServices(String input, String output) {
        List<Service> serviceList = new ArrayList<>();
        
        for (Map.Entry<String, Service> entry: serviceMap.entrySet()) {
            if (entry.getValue().getOutput().equalsIgnoreCase(output)) {
                serviceList.add(entry.getValue());
            }
        }
        return serviceList;
    }

    public void updateQOS(Map<String, QOS[]> qosMap) {
        for (Map.Entry<String, QOS[]> entry: qosMap.entrySet()) {
            serviceMap.get(entry.getKey()).updateQOS(entry.getValue());
        }
    }
}
