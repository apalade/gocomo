package decentralisedmodel.model;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class Service implements Comparable<Service> {

    private String id;
    private Map<String, QOS> qosLevel;
    private String serviceClass;
    private double utility;

    // Input and output parameters
    private String input;
    private String output;
    
    public Service(String serviceID, String servClass, String input, String output, Map<String, QOS> qos) {
        id = serviceID;
        qosLevel = qos;
        serviceClass = servClass;
        this.input = input;
        this.output = output;
    }

//    public Service(String serviceID, Map<String, Double> qos) {
//        this(serviceID, null, null, qos);
//    }
//    
//    public Service(Map<String, Double> qos) {
//        this(UUID.randomUUID().toString(), null, null, qos);
//    }
    
    public Service(Map<String, QOS> qos, String serviceClass, String input, String output) {
        this(UUID.randomUUID().toString(),serviceClass, input, output, qos);
        
    }

    public Service(String serviceID, Map<String, QOS> qos, String serviceClass) {
        this.serviceClass = serviceClass;
        id = serviceID;
        qosLevel = qos;
    }
    
    public String getID() {
        return id;
    }

    public Map<String, QOS> getQOSLevel() {
        return qosLevel;
    }

    public String getServiceClass() {
        return serviceClass;
    }

    public void setServiceClass(String serviceClass) {
        this.serviceClass = serviceClass;
    }

    public double getUtility() {
        return utility;
    }

    public void setUtility(double utility) {
        this.utility = utility;
    }

//    public QOS[] toQOSArray() {
//        QOS[] qosArray = new QOS[qosLevel.size()];
//        return qosLevel.toArray(qosArray);
//    }
    
    /**
     * Retrieves the QoS value for a given QoS property
     *
     * @param qosName The name of the property we're looking for
     * @return The QoS value
     */
    public double getQoSValue(String qosName) {
        return qosLevel.get(qosName).getValue();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(id).append(",").append(serviceClass).append(",");
        
        for(Map.Entry<String, QOS> entry: qosLevel.entrySet()) {
            builder.append(entry.getKey()).append(":").append(entry.getValue().getValue()).append(" ");
        }
        
        return builder.toString().trim();
    }

    @Override
    public int compareTo(Service o) {
        if (this.getUtility() > o.getUtility()) {
            return 1;
        } else if (this.getUtility() == o.getUtility()) {
            return 0;
        } else {
            return -1;
        }
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
    
    // update the qos values 
    public void updateQOS(Map<String, QOS> qosMap) {
        for(Map.Entry<String, QOS> entry: qosMap.entrySet()) {
            qosLevel.put(entry.getKey(), entry.getValue());
        }
    }
    
    // update the qos values 
    public void updateQOS(QOS[] qosList) {
        for(QOS qos: qosList) {
            qosLevel.put(qos.getName(), qos);
        }
    }
}
