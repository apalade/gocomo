package decentralisedmodel.data;

import decentralisedmodel.model.QOS;
import decentralisedmodel.model.Service;
import decentralisedmodel.util.QoSUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * This is the class which produces the input data
 * 
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class InputData {
    
    
    // The hard goals
    public static String[] queries = new String[]{"Providers", "KRoutes"};
    
    // The abstract tasks (process variability
    public static String[] serviceClasses = {"RANDOMK", "TOPK", "SP1", "SP2" };

    public static double[] responseTime = {10.0, 20.0, 30.0, 40.0, 45.5, 47.5, 80.0, 90.0, 10.0, 20.0, 30.0, 40.0, 45.5, 47.5, 80.0, 90.0, 10.0, 20.0, 30.0, 40.0, 45.5, 47.5, 80.0, 90.0};
    public static double[] price = {50.0, 220.0, 30.0, 40.0, 50.0, 60.0, 65.6, 78.0, 210.0, 220.0, 30.0, 40.0, 50.0, 60.0, 65.6, 78.0, 230.0, 120.0, 30.0, 40.0, 50.0, 60.0, 65.6, 78.0};
    public static double[] throughput = {150.1, 250.2, 134.1, 500.4, 200.9, 111.1, 23.4, 25.9, 99.9, 44.4, 134.1, 500.4, 200.9, 111.1, 23.4, 25.9, 99.9, 44.4, 200.9, 111.1, 23.4, 25.9, 99.9, 44.4};

    public static boolean isRTPositive = false;  // needs to be minimised
    public static boolean isPricePositive = false;
    public static boolean isTPPositive = true;  // needs to be maximised

    public static double TP_CONSTRAINT = 130.3; // Lower bound - we want to maximise this -> The higher the better
    public static double PRICE_CONSTRAINT = 60.0; //Upper bound - we want to minimise these
    public static double RT_CONSTRAINT = 24.0; //Upper bound 

    public static double TP_GLOBAL_MIN = 10.0;
    public static double PRICE_GLOBAL_MIN = 10.0;  //Lowest
    public static double RT_GLOBAL_MIN = 30.0;

    public static double TP_GLOBAL_MAX = 1000.0;
    public static double PRICE_GLOBAL_MAX = 200.0;  //Highest
    public static double RT_GLOBAL_MAX = 100.0;

    public static double TP_WEIGHT = 0.1;
    public static double PRICE_WEIGHT = 0.1;
    public static double RT_WEIGHT = 0.8;
    
    public static String[][] inputOutput = {{"a", "b"}, {"a", "c"}, {"b", "d"}, {"c", "d"}};

    // Must add the service class if we want to extend this.
//    public static List<Service> createServices() {
//
//        List<Service> serviceList = new ArrayList<>();
//
//        for (int i = 0; i < responseTime.length; i++) {
//            List<QOS> qos = new ArrayList<>();
//            qos.add(new QOS(QoSUtils.RESPONSE_TIME, responseTime[i], isRTPositive));
//            qos.add(new QOS(QoSUtils.PRICE, price[i], isPricePositive));
//            qos.add(new QOS(QoSUtils.THROUGHPUT, throughput[i], isTPPositive));
//            serviceList.add(new Service(Integer.toString(i), qos));
//        }
//
//        return serviceList;
//    }

    // Must add the service class if we want to extend this.
//    public static List<Service> createServices(String serviceClass) {
//
//        List<Service> serviceList = new ArrayList<>();
//
//        for (int i = 0; i < responseTime.length; i++) {
//            List<QOS> qos = new ArrayList<>();
//            qos.add(new QOS(QoSUtils.RESPONSE_TIME, responseTime[i], isRTPositive));
//            qos.add(new QOS(QoSUtils.PRICE, price[i], isPricePositive));
//            qos.add(new QOS(QoSUtils.THROUGHPUT, throughput[i], isTPPositive));
//            serviceList.add(new Service(Integer.toString(i), qos, serviceClass));
//        }
//
//        return serviceList;
//    }
//    
      // Must add the service class if we want to extend this.
    public static List<Service> createServices(String serviceClass, String input, String output) {

        List<Service> serviceList = new ArrayList<>();

        for (int i = 0; i < responseTime.length; i++) {
            Map<String, QOS> map = new HashMap<>();
            map.put(QoSUtils.RESPONSE_TIME,new QOS(QoSUtils.RESPONSE_TIME, responseTime[i], isRTPositive));
            map.put(QoSUtils.PRICE,new QOS(QoSUtils.PRICE, price[i], isPricePositive));
            map.put(QoSUtils.THROUGHPUT,new QOS(QoSUtils.THROUGHPUT, throughput[i], isTPPositive));
            serviceList.add(new Service(map, serviceClass, input, output));
        }

        return serviceList;
    }
    
    // Must add the service class if we want to extend this.
//    public static Map<String, List<Service>> createServices(String[] serviceClassList) {
//
//        Random random = new Random();
//        
//        Map<String, List<Service>> serviceMap = new HashMap<>();
//        
//        for (String serviceClass: serviceClassList) {
//            
//            List<Service> serviceList = new ArrayList<>();
//
//            for (int i = 0; i < responseTime.length; i++) {
//                List<QOS> qos = new ArrayList<>();
//                
//                // Generate some Random QOS
//                qos.add(new QOS(QoSUtils.RESPONSE_TIME, random.nextDouble() * responseTime[i], isRTPositive));
//                qos.add(new QOS(QoSUtils.PRICE, random.nextDouble() * price[i], isPricePositive));
//                qos.add(new QOS(QoSUtils.THROUGHPUT, random.nextDouble() * throughput[i], isTPPositive));
//                serviceList.add(new Service(UUID.randomUUID().toString(), qos, serviceClass));
//            }
//            serviceMap.put(serviceClass, serviceList);
//        }
//        
//        return serviceMap;
//    }
    
    public static Map<String, QOS> getConstraints() {
        Map<String, QOS> map = new HashMap<>();
        map.put(QoSUtils.RESPONSE_TIME, new QOS(QoSUtils.RESPONSE_TIME, RT_CONSTRAINT, isRTPositive));
        map.put(QoSUtils.PRICE, new QOS(QoSUtils.PRICE, PRICE_CONSTRAINT, isPricePositive));
        map.put(QoSUtils.THROUGHPUT, new QOS(QoSUtils.THROUGHPUT, TP_CONSTRAINT, isTPPositive));
        return map;
    }
    
    public static QOS[] getConstraintsAsArray() {
        QOS rt = new QOS(QoSUtils.RESPONSE_TIME, RT_CONSTRAINT, isRTPositive);
        QOS price = new QOS(QoSUtils.PRICE, PRICE_CONSTRAINT, isPricePositive);
        QOS tp = new QOS(QoSUtils.THROUGHPUT, TP_CONSTRAINT, isTPPositive);

        QOS[] qos = new QOS[3];
        qos[0] = rt;
        qos[1] = price;
        qos[2] = tp;
        
        return qos;
    }

    public static Map<String, Double> getMin() {
        Map<String, Double> map = new HashMap<>();
        map.put(QoSUtils.RESPONSE_TIME, RT_GLOBAL_MIN);
        map.put(QoSUtils.PRICE, PRICE_GLOBAL_MIN);
        map.put(QoSUtils.THROUGHPUT, TP_GLOBAL_MIN);
        return map;
    }

    public static Map<String, Double> getMax() {
        Map<String, Double> map = new HashMap<>();
        map.put(QoSUtils.RESPONSE_TIME, RT_GLOBAL_MAX);
        map.put(QoSUtils.PRICE, PRICE_GLOBAL_MAX);
        map.put(QoSUtils.THROUGHPUT, TP_GLOBAL_MAX);
        return map;
    }

    public static Map<String, Double> getWeights() {
        Map<String, Double> map = new HashMap<>();
        map.put(QoSUtils.RESPONSE_TIME, RT_WEIGHT);
        map.put(QoSUtils.PRICE, PRICE_WEIGHT);
        map.put(QoSUtils.THROUGHPUT, TP_WEIGHT);
        return map;
    }
    
    public static List<String> serviceClassesAsList() {
        List<String> serviceClassesList = new ArrayList<>();
        for (String serviceClass: serviceClasses) {
            serviceClassesList.add(serviceClass);
        }
        return serviceClassesList;
    }
    
    
//    public static List<SatisfactionLabel> getSatisfactionLabels() {
//        // The importance
//        List<SatisfactionLabel> labels = new ArrayList<>();
//        labels.add(new SatisfactionLabel("PRICE", 10));
//        labels.add(new SatisfactionLabel("RT", 10));
//        return labels;
//    }
//    
//    public static List<Rank> getRanksAsList() {
//        List<Rank> ranks = new ArrayList<>();
//        ranks.add(new Rank(QoSUtils.RESPONSE_TIME, 2));
//        ranks.add(new Rank(QoSUtils.PRICE, 5));
//        ranks.add(new Rank(QoSUtils.THROUGHPUT, 1));
//        return ranks;
//    }
    
    public static double alphaIndex = -0.05;
    
    public static int intervals = 5;
}
